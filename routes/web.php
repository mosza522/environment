<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(!Auth::guest()){
        return redirect('/backoffice');
    }else{
        return view('backoffice.login');
    }
});

Route::group(['middleware' => 'auth'], function (){
    Route::get('/backoffice', function () {
      Session::put('page','Dashboard');
          return view('backoffice.dashboard');
    })->middleware('admin');

    Route::get('/backoffice/user', function () {
      Session::put('page','User');
        return view('backoffice.user');
    })->middleware('admin');

    Route::get('/backoffice/faq', function () {
      Session::put('page','Faq');
        return view('backoffice.Content.faq');
    })->middleware('admin');
    Route::get('/backoffice/terms', function () {
      Session::put('page','terms');
        return view('backoffice.Content.terms');
    })->middleware('admin');

  Route::post('/UserController/{id}/update', 'UserController@update');
  Route::resource('/UserController', 'UserController');
  // content controller
  Route::post('/ContentController/{type}', 'ContentController@store');

  // upload images forala
  Route::post('upload_image/{type}','FroalaController@uploads');

});

#-------------Auth----------

Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
// Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
