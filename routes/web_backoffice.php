<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/loginBackoffice', function () {
         return view('backoffice.login');
    });
Route::group(['middleware' => 'auth'], function (){
    
      Route::get('/backoffice', function () {
          if(!Auth::guest() or Auth::user()->type=="Admin"){
                return view('backoffice.dashboard');
          }else{
              return redirect('loginBackoffice');
          }
    });
    // else{
    //     return redirect("/loginUser");
    // }
});