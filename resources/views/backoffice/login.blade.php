<!DOCTYPE html>
<html>
    <head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!-- Favicons -->
  <link rel="apple-touch-icon" href="{{asset('assets/img/apple-icon.png')}}">
  <link rel="icon" href="{{asset('assets/img/favicon.png')}}">
  <title>
      Seektour | LOGIN
  </title>
 <!-- style -->
     @extends('backoffice.layouts.inc_head')
     <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/core/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-material-design.js')}}"></script>
<script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
<!--  Google Maps Plugin  -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin  -->
<script src="{{asset('assets/js/plugins/moment.min.js')}}"></script>
<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="{{asset('assets/js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('assets/js/plugins/nouislider.min.js')}}"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{asset('assets/js/plugins/bootstrap-selectpicker.js')}}"></script>
<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/  -->
<script src="{{asset('assets/js/plugins/bootstrap-tagsinput.js')}}"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{asset('assets/js/plugins/jasny-bootstrap.min.js')}}"></script>
<!-- Plugins for presentation and navigation  -->
<script src="{{asset('assets/assets-for-demo/js/modernizr.js')}}"></script>
<!-- Material Dashboard Core initialisations of plugins and Bootstrap Material Design Library -->
<script src="{{asset('assets/js/material-dashboard.js?v=2.0.1')}}"></script>
<!-- Dashboard scripts -->
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="{{asset('assets/js/plugins/arrive.min.js')}}" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="{{asset('assets/js/plugins/jquery.validate.min.js')}}"></script>
<!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
<script src="{{asset('assets/js/plugins/chartist.min.js')}}"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="{{asset('assets/js/plugins/jquery.bootstrap-wizard.js')}}"></script>
<!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
<script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="{{asset('assets/js/plugins/jquery-jvectormap.js')}}"></script>
<!-- Sliders Plugin, full documentation here: https://refreshless.com/nouislider/ -->
<script src="{{asset('assets/js/plugins/nouislider.min.js')}}"></script>
<!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{asset('assets/js/plugins/jquery.select-bootstrap.js')}}"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
<script src="{{asset('assets/js/plugins/jquery.datatables.js')}}"></script>
<!-- Sweet Alert 2 plugin, full documentation here: https://limonte.github.io/sweetalert2/ -->
<script src="{{asset('assets/js/plugins/sweetalert2.js')}}"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{asset('assets/js/plugins/jasny-bootstrap.min.js')}}"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="{{asset('assets/js/plugins/fullcalendar.min.js')}}"></script>
<!-- demo init -->
<script src="{{asset('assets/js/plugins/demo.js')}}"></script>
 </head>

<body class="off-canvas-sidebar login-page">

  @if ($errors->has('credentials'))
  <script>
      swal("Something Wrong!!", "{{ $errors->first('credentials') }}", "warning");
  </script>
  @endif
  @if ($errors->has('username'))
  <script>
      swal("Something Wrong!!", "{{ $errors->first('username') }}", "warning");
  </script>
  @endif
  @if(Session::has('wrong'))
  {{--  {{ dd($error) }}  --}}
    <script>
       swal("มีบางอย่างผิดพลาด", "{{ Session::get('wrong') }}", "error");
   </script>
   @endif
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg bg-primary navbar-transparent navbar-absolute" color-on-scroll="500">
    <div class="container">
      <div class="col-md-12 text-center">
        <a class="navbar-brand" href="#pablo">Seektour Backoffice</a>
      </div>
      <!--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">-->
      <!--  <span class="sr-only">Toggle navigation</span>-->
      <!--  <span class="navbar-toggler-icon icon-bar"></span>-->
      <!--  <span class="navbar-toggler-icon icon-bar"></span>-->
      <!--  <span class="navbar-toggler-icon icon-bar"></span>-->
      <!--</button>-->
      <!--<div class="collapse navbar-collapse justify-content-end" id="navbar">-->
      <!--  <ul class="navbar-nav">-->
      <!--    <li class="nav-item">-->
      <!--      <a href="../dashboard.html" class="nav-link">-->
      <!--        <i class="material-icons">dashboard</i> Dashboard-->
      <!--      </a>-->
      <!--    </li>-->
      <!--    <li class="nav-item ">-->
      <!--      <a href="register.html" class="nav-link">-->
      <!--        <i class="material-icons">person_add</i> Register-->
      <!--      </a>-->
      <!--    </li>-->
      <!--    <li class="nav-item  active ">-->
      <!--      <a href="login.html" class="nav-link">-->
      <!--        <i class="material-icons">fingerprint</i> Login-->
      <!--      </a>-->
      <!--    </li>-->
      <!--    <li class="nav-item ">-->
      <!--      <a href="lock.html" class="nav-link">-->
      <!--        <i class="material-icons">lock_open</i> Lock-->
      <!--      </a>-->
      <!--    </li>-->
      <!--  </ul>-->
      <!--</div>-->
    </div>
  </nav>
  <!-- End Navbar -->
  @php
  $user=App\Models\User::first();
  @endphp

  <div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('{{asset('assets/img/login.jpg')}}'); background-size: cover; background-position: top center;">
      <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
      <div class="container">
        <div class="col-md-4 col-sm-6 ml-auto mr-auto">
          <div id="status">
          </div>
          <img src="" id="img" alt="">
          <form class="form" method="post" action="login">
              {{ csrf_field() }}
            <div class="card card-login card-hidden">
              <div class="card-header card-header-rose text-center">
                <h4 class="card-title">Log in</h4>

                <!--<div class="social-line">-->
                <!--  <a href="#pablo" class="btn btn-just-icon btn-link btn-white">-->
                <!--    <i class="fa fa-facebook-square"></i>-->
                <!--  </a>-->
                <!--  <a href="#pablo" class="btn btn-just-icon btn-link btn-white">-->
                <!--    <i class="fa fa-twitter"></i>-->
                <!--  </a>-->
                <!--  <a href="#pablo" class="btn btn-just-icon btn-link btn-white">-->
                <!--    <i class="fa fa-google-plus"></i>-->
                <!--  </a>-->
                <!--</div>-->
              </div>
              <div class="card-body ">
                <!--<p class="card-description text-center">Or Be Classical</p>-->
                <span class="bmd-form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">person</i>
                      </span>
                    </div>
                    <input type="text" name="username" class="form-control" placeholder="Username..." required autofocus>
                  </div>
                </span>
                <!--<span class="bmd-form-group">-->
                <!--  <div class="input-group">-->
                <!--    <div class="input-group-prepend">-->
                <!--      <span class="input-group-text">-->
                <!--        <i class="material-icons">email</i>-->
                <!--      </span>-->
                <!--    </div>-->
                <!--    <input type="email" class="form-control" placeholder="Email...">-->
                <!--  </div>-->
                <!--</span>-->
                <span class="bmd-form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                    <input type="password" name="password" class="form-control" placeholder="Password..." required>
                  </div>
                </span>
              </div>
              <div class="card-footer justify-content-center">
                <button class="btn btn-rose btn-link btn-lg" type="submit"> LETS GO</button>
                {{-- <div class="fb-login-button" data-max-rows="1" data-size="medium" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" onlogin="checkLoginState();"></div> --}}
                {{-- <button class="btn btn-facebook" scope="public_profile,email" type="button" onclick="login();" >
                  <i class="fa fa-facebook"> </i> Login with Facebook
                </button>
                <fb:login-button style="" class="btn btn-facebook" scope="public_profile,email" onlogin="checkLoginState();" id="fb_login">
                <i class="fa fa-facebook"> </i> Login with Facebook
            </fb:login-button> --}}
              </div>
            </div>
          </form>
        </div>
      </div>
      <footer class="footer ">
        <div class="container">
          <nav class="pull-left">
            <ul>
              <li>
                <a href="https://www.creative-tim.com">
                  Creative Tim
                </a>
              </li>
              <li>
                <a href="http://presentation.creative-tim.com">
                  About Us
                </a>
              </li>
              <li>
                <a href="http://blog.creative-tim.com">
                  Blog
                </a>
              </li>
              <li>
                <a href="https://www.creative-tim.com/license">
                  Licenses
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright pull-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, We are Tesseract warp you to anywhere on the galaxy. | Seektour Co.,Ltd
          </div>
        </div>
      </footer>
    </div>
  </div>


</body>
<script type="text/javascript">
  $(document).ready(function() {
    demo.checkFullPageBackgroundImage();
    setTimeout(function() {
      // after 1000 ms we add the class animated to the login/register card
      $('.card').removeClass('card-hidden');
    }, 700)
  });

</script>
{{-- Facebook  --}}
<script>
window.fbAsyncInit = function() {
  FB.init({
    appId      : '476676269441875',
    cookie     : true,
    xfbml      : true,
    version    : 'v3.0'
  });

  FB.AppEvents.logPageView();
};
function checkLoginState() {
  FB.login(function(response) {
    statusChangeCallback(response);
  });
}
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

   function statusChangeCallback(response) {
     if(response.status==='connected'){
       testAPI();
       console.log('Logged in');
     }else{
       console.log('Not authenticated');
     }
   }

   function testAPI() {
     console.log('Welcome!  Fetching your information.... ');
     FB.api('/me', { locale: 'en_US', fields: 'name, email,picture' },
     function(response) {
       console.log('Successful login for: ' + response.name);
       document.getElementById('status').innerHTML =
       'Thanks for logging in, '+ response.name + ' Email : ' + response.email + '!';
       console.log(response.picture.data.url);
       $('#img').prop("src",response.picture.data.url);
     });
   }
   function login() {
     $('#fb_login').trigger("click");
   }

</script>
</html>
