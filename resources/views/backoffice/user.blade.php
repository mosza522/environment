@extends('backoffice.layouts.main')
@section('title')
  User || Seektour
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
      <button type="button" class="btn btn-primary btn-round add" >
        <i class="material-icons">person_add</i> เพิ่ม Admin
      </button>
    </div>
  </div>
  <div class="row">
    {{-- <div class="col-md-4"></div> --}}
    <div class="col-md-12">
      <form id="add_admin" enctype="multipart/form-data" method="post" >
        {{ csrf_field() }}
        <div class="card ">
          <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon">
              <i class="material-icons">account_box</i>
            </div>
            <h4 class="card-title">ข้อมูล Admin</h4>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="username" class="bmd-label-floating"> Username *</label>
                  <input type="text" class="form-control" id="username" name="username" required minLength="6">
                  <label> กรุณากรอกอย่างน้อย 6 ตัวอักษร *</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="email" class="bmd-label-floating"> Email Address *</label>
                  <input type="email" class="form-control" id="email" name="email" required="true" email="true">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="Password" class="bmd-label-floating"> Password *</label>
                  <input type="password" class="form-control" id="Password" required="true" name="password">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="password_con" class="bmd-label-floating"> Confirm Password *</label>
                  <input type="password" class="form-control" id="password_con" required="true" equalTo="#Password" name="password_confirmation">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="type" class="bmd-label-floating"> Admin Level*</label>
                  <div class="col-lg-5 col-md-6 col-sm-3">
                    <select class="selectpicker" data-size="7" data-style="btn btn-primary btn-round" name="type" id="type" title="Choose Admin Level" required>
                      <option value="Admin">Admin</option>
                      <option value="SuperAdmin">SuperAdmin</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="image" class="bmd-label-floating title"> Images *</label>
                  <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle">
                      <img src="{{asset('assets/img/placeholder.jpg')}}" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
                    <div>
                      <span class="btn btn-round btn-rose btn-file">
                        <span class="fileinput-new">Add Photo</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image" id="image" required/>
                      </span>
                      <br />
                      <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="fullname" class="bmd-label-floating"> Name *</label>
                  <input type="text" class="form-control" id="fullname" name="fullname" required="true">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="birthday" class="bmd-label-floating"> Birthday *</label>
                  <input type="text" class="datepicker" id="birthday" name="birthday" value=""  autocomplete="off" style="border-top: none;border-left: none;border-right: none;width:100%;">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
            <div class="form-group">
              <label for="gender" class="bmd-label-floating"> gender *</label>
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="radio" name="gender" value="Male" checked> ชาย
                  <span class="circle">
                    <span class="check"></span>
                  </span>
                </label>
              </div>
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="radio" name="gender" value="Female" > หญิง
                  <span class="circle">
                    <span class="check"></span>
                  </span>
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="tel" class="bmd-label-floating"> tel *</label>
              <input type="text" class="form-control" name="tel" id="tel" autocomplete="off" required="true" onkeypress="return check_number(this)" number="true">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="address" class="bmd-label-floating">Address *</label>
              <textarea class="form-control" id="address" name="address" rows="3" required></textarea>
            </div>
          </div>
        </div>
            <div class="category form-category">* Required fields</div>

          <div class="card-footer">
            <div class="col-md-12 text-right">
              <button type="button" class="btn btn-danger btn-round close-add" >Close</button>
              <button type="submit" class="btn btn-success btn-round">บันทึก</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  </div>
  {{-- edit  --}}
  <div class="row">
    {{-- <div class="col-md-4"></div> --}}
    <div class="col-md-12">
      <form id="edit_admin" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <div class="card ">
          <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon">
              <i class="material-icons">account_box</i>
            </div>
            <h4 class="card-title">แก้ไขข้อมูล Admin</h4>
          </div>
          <div class="card-body ">
            <div class="row">
              <div class="col-md-6">
                <input type="hidden" name="id" id="id_hidden" value="">
                <div class="form-group">
                  <label for="username_edit" class="bmd-label-floating"> Username *</label>
                  <input type="text" class="form-control" id="username_edit" name="username" required minLength="6">
                  <label> กรุณากรอกอย่างน้อย 6 ตัวอักษร *</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="email_edit" class="bmd-label-floating"> Email Address *</label>
                  <input type="email" class="form-control" id="email_edit" name="email" required="true" email="true">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="type_edit" class="bmd-label-floating"> Admin Level*</label>
                  <div class="col-lg-5 col-md-6 col-sm-3">
                    <select class="selectpicker" data-size="7" data-style="btn btn-primary btn-round" name="type" id="type_edit" required>
                      {{-- <option value="xx">xxx</option> --}}
                      <option value="Admin" id="Admin">Admin</option>
                      <option value="SuperAdmin" id="SuperAdmin">SuperAdmin</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="image" class="bmd-label-floating title"> Images *</label>
                  <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle">
                      <img src="{{asset('assets/img/placeholder.jpg')}}" alt="..." id="image_edit">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
                    <div>
                      <span class="btn btn-round btn-rose btn-file">
                        <span class="fileinput-new">Add Photo</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image" id="image" />
                      </span>
                      <br />
                      <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="fullname_edit" class="bmd-label-floating"> Name *</label>
                  <input type="text" class="form-control" id="fullname_edit" name="fullname" required="true">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="birthday_edits" class="bmd-label-floating"> Birthday *</label>
                  {{-- Error minibar  --}}
                  <input type="text" class="form-control datepicker" id="birthday_edit" name="birthday" value=""  autocomplete="off" >
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
            <div class="form-group">
              <label for="gender" class="bmd-label-floating"> gender *</label>
              <div class="form-check">
                <label class="form-check-label" >
                  <input class="form-check-input" type="radio" name="gender" id="Male"  value="Male"> ชาย
                  <span class="circle">
                    <span class="check" ></span>
                  </span>
                </label>
              </div>
              <div class="form-check">
                <label class="form-check-label" >
                  <input class="form-check-input" type="radio" name="gender" id="Female" value="Female" > หญิง
                  <span class="circle">
                    <span class="check" ></span>
                  </span>
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="tel" class="bmd-label-floating"> tel *</label>
              <input type="text" class="form-control" name="tel" id="tel_edit" autocomplete="off" onkeypress="return check_number(this)" required="true" number="true">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="address" class="bmd-label-floating">Address *</label>
              <textarea class="form-control" id="address_edit" name="address" rows="3" required></textarea>
            </div>
          </div>
        </div>
            <div class="category form-category">* Required fields</div>

          <div class="card-footer">
            <div class="col-md-12 text-right">
              <button type="button" class="btn btn-danger btn-round close-edit" >Close</button>
              <button type="submit" class="btn btn-success btn-round">บันทึก</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <!-- Button trigger modal -->
      <div class="card">
        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <i class="material-icons">person</i>
          </div>
          <h4 class="card-title">รายชื่อผู้ใช้ทั้งหมด</h4>
        </div>
        <div class="card-body">
          <div class="toolbar">
            <!--        Here you can write extra buttons/actions for the toolbar              -->
          </div>
          <div class="material-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
              <thead class=".thead-light">
                <tr>
                  <th>#</th>
                  <th>Username</th>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Tel</th>
                  <th>Img</th>
                  <th>Gender</th>
                  <th>Created at</th>
                  <th class="disabled-sorting text-right">Actions</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>#</th>
                  <th>Username</th>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Tel</th>
                  <th>Img</th>
                  <th>Gender</th>
                  <th>Created at</th>
                  <th class="text-right">Actions</th>
                </tr>
              </tfoot>
              @php
                $user=\App\Models\User::orderBy('id','DESC')->get();
                $i=1;
                // dd($user);
              @endphp
              <tbody>
                @foreach ($user as $key )
                <tr>
                  <th>{{$i++}}</th>
                  <th>{{$key->username}}</th>
                  <th>{{$key->name}}</th>
                  <th>{{$key->type}}</th>
                  <th>{{$key->email}}</th>
                  <th>{{$key->address}}</th>
                  <th>{{$key->tel}}</th>
                  <th>
                    {{-- <div class="tz-gallery">
                    <a class="lightbox" href="{{$key->img}}">
                    <img class="img-thumbnail" src="{{$key->img}}" alt="Coast">
                  </a>
                </div> --}}
                @if (Storage::disk('s3')->has('img-user/'.$key->img))
                  <div class="tz-gallery">
                    <a class="lightbox" href="{{Storage::disk('s3')->url('img-user/'.$key->img)}}">
                      <img class="img-thumbnail" src="{{Storage::disk('s3')->url('img-user/'.$key->img)}}" alt="Coast">
                    </a>
                  @endif
                </div>
                </th>
                  <th>{{$key->gender}}</th>
                  <th>{{$key->created_at}}</th>
                  <td class="text-right">
                    <a href="#" class="btn btn-link btn-warning btn-just-icon edit" onclick="return show({{$key->id}})"><i class="material-icons">dvr</i></a>
                    <a href="#" class="btn btn-link btn-danger btn-just-icon" onclick="return del({{$key->id}})"><i class="material-icons">close</i></a>
                  </td>
                </tr>
                @endforeach
                </tbody>
            </table>
          </div>
        </div>
        <!-- end content-->
      </div>
      <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
  </div>

  <!-- Modal -->
  {{-- <div class="modal fade" id="ModalInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width:70%">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">เพิ่ม Admin </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

        </div>
        <div class="modal-body">
          <form id="Validate" enctype="multipart/form-data">
            <div class="card ">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">account_box</i>
                </div>
                <h4 class="card-title">ข้อมูล Admin</h4>
              </div>
              <div class="card-body ">
                <div class="form-group">
                  <label for="username" class="bmd-label-floating"> Username *</label>
                  <input type="text" class="form-control" id="username" name="username" required="true">
                </div>
                <div class="form-group">
                  <label for="email" class="bmd-label-floating"> Email Address *</label>
                  <input type="email" class="form-control" id="email" name="email" required="true">
                </div>
                <div class="form-group">
                  <label for="Password" class="bmd-label-floating"> Password *</label>
                  <input type="password" class="form-control" id="Password" required="true" name="password">
                </div>
                <div class="form-group">
                  <label for="password_con" class="bmd-label-floating"> Confirm Password *</label>
                  <input type="password" class="form-control" id="password_con" required="true" equalTo="#Password" name="password_confirmation">
                </div>
                <div class="form-group">
                  <label for="type" class="bmd-label-floating"> Admin Level*</label>
                  <div class="col-lg-5 col-md-6 col-sm-3">
                    <select class="selectpicker" data-size="7" data-style="btn btn-primary btn-round" id="type" title="Admin Level" required>
                      <option value="Admin">Admin</option>
                      <option value="SuperAdmin">SuperAdmin</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <h4 class="bmd-label-floating">Regular Image</h4>
                  <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-circle">
                      <img src="{{asset('assets/img/placeholder.jpg')}}" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
                    <div>
                      <span class="btn btn-round btn-rose btn-file">
                        <span class="fileinput-new">Add Photo</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="..." />
                      </span>
                      <br />
                      <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="bmd-label-floating"> Name *</label>
                  <input type="text" class="form-control" id="name" required="true">
                </div>
                <div class="form-group">
                  <label for="name" class="bmd-label-floating"> Birthday *</label>
                  <input type="text" class="form-control datepicker" value="10/05/2016">
                </div>

                <div class="form-group">
                  <label for="name" class="bmd-label-floating"> gender *</label>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="gender" value="male" checked> ชาย
                      <span class="circle">
                        <span class="check"></span>
                      </span>
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="gender" value="female" > หญิง
                      <span class="circle">
                        <span class="check"></span>
                      </span>
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="tel" class="bmd-label-floating"> tel *</label>
                  <input type="text" class="form-control" id="tel" id="tel" required="true">
                </div>
                <div class="form-group">
                  <label for="address">Address *</label>
                </div>
                <div class="form-group">
                  <textarea class="form-control" id="address" name="address" rows="3" required></textarea>
                </div>
                <div class="category form-category">* Required fields</div>
              </div>
              <div class="card-footer">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success btn-round">บันทึก</button>
        </div>
        </form>
        </div>
    </div>
  </div> --}}
  <script type="text/javascript">
  baguetteBox.run('.tz-gallery');
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }


      });


      var table = $('#datatables').DataTable();

      // Edit record
      // table.on('click', '.edit', function() {
      //   $tr = $(this).closest('tr');
      //
      //   var data = table.row($tr).data();
      //   alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      // });

      // Delete a record
      // table.on('click', '.remove', function(e) {
      //   $tr = $(this).closest('tr');
      //   table.row($tr).remove().draw();
      //   e.preventDefault();
      // });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });

      $('.card .material-datatables label').addClass('form-group');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
  md.initFormExtendedDatetimepickers();
  setFormValidation('#add_admin');
  setFormValidation('#edit_admin');
  // $('#add_admin').validate();
  $('#add_admin').hide();
  $('.add').click(function(){
    $('.add').fadeOut(500);
    $('#add_admin').fadeIn(1000);
    $('#edit_admin').hide();
  });
  $('.close-add').click(function(){
    $('#add_admin').fadeOut(500);
    $('.add').show();
  });

  $('#edit_admin').hide();
  // $('.edit').click(function(){
    // $('.add').show();
    // $('#add_admin').hide();
    // $('#edit_admin').fadeIn(1000);
  // });
  $('.close-edit').click(function(){
    $('#edit_admin').fadeOut(500);
  });

});
  function setFormValidation(id){
    $(id).validate({
      highlight: function(element) {
          $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
          return false;
          // $(element.id).append('<span class="form-control-feedback"><i class="material-icons">done</i></span>');

      },
      success: function(element) {
          $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
          // $(element.id).append('<span class="form-control-feedback"><i class="material-icons">clear</i></span>');
      },
      errorPlacement : function(error, element) {
          $(element).append(error);
      },
    });
  }
</script>
<script type="text/javascript">
$.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
   });
$('#add_admin').submit(function(){
  var fields = $('#add_admin').find("select, textarea, input").serializeArray();
  var required=false;
  $.each(fields, function(i, field) {
    if (field.value==""){
      console.log(field);
      required=true;
    }
  });
  if(required){
    console.log('false');
    return false;
  }
  $('#divLoading').addClass('show');
  $('#background').addClass('show');
  var formData = new FormData($(this)[0]);
  var table = $('#datatables').DataTable();
  $.ajax({
    url: '{{url('UserController')}}',
    type: 'POST',
    data: formData,
    dataType:'json',
    success : function (response) {
      // console.log(response);
      $('#divLoading').removeClass('show');
      $('#background').removeClass('show');
      swal("Successfully", "เพิ่ม Admin เสร็จสิ้น", "success");
      table.clear();
      table.rows.add(response).draw();
      $('#add_admin').hide();
      $('.add').show();
      baguetteBox.run('.tz-gallery');
      $('#add_admin')[0].reset();
    },
    cache: false,
    contentType: false,
    processData: false
  });
  return false;
});
function check_number(number) {
  var vchar = String.fromCharCode(event.keyCode);
  if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
  number.onKeyPress=vchar;

}
function del(id) {

  var dataTable = $("#datatables").DataTable();
  swal({
      title: 'ยืนยันการลบข้อมูล ?',
      text: 'เมื่อลบแล้วไม่สามารถกู้ข้อมูลคืนได้ !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'ยืนยัน !',
      cancelButtonText: 'ยกเลิก',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
  }).then(function() {
    $('#divLoading').addClass('show');
    $('#background').addClass('show');
    var table = $('#datatables').DataTable();
    $.ajax({
      url: '{{url('UserController')}}/'+id,
      type: 'DELETE',
      dataType:'json',
      success : function (response) {
        $('#divLoading').removeClass('show');
        $('#background').removeClass('show');
        // alert(JSON.stringify(response));
        // alert(response);
        swal({
            title: 'ลบข้อมูลแล้ว!',
            text: 'ข้อมูลนี้ถูกลบแล้ว',
            type: 'success',
            confirmButtonClass: "btn btn-success",
            buttonsStyling: false
        }).catch(swal.noop)
        table.clear();
        table.rows.add(response).draw();
        baguetteBox.run('.tz-gallery');
      },
      cache: false,
      contentType: false,
      processData: false
    });
    return false;

  }, function(dismiss) {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
          swal({
              title: 'ยกเลิกการลบข้อมูล ',
              text: 'ข้อมูลนี้ยังปลอดภัย :)',
              type: 'error',
              confirmButtonClass: "btn btn-info",
              buttonsStyling: false
          }).catch(swal.noop)
      }
  })
}
function show(id) {

  $('#edit_admin')[0].reset();
  $('#edit_admin').hide();
  $('.add').show();
  $('#add_admin').hide();
  $('#edit_admin').fadeIn(1000);
  $('.form-group').removeClass("has-success");
  $.ajax({
    url: '{{url('UserController')}}/'+id,
    type: 'get',
    dataType:'json',
    success : function (response) {
      console.log(response);
      //Get the text using the value of select
      var text = $("select[id=type_edit] option[value='"+response.type+"']").text();
      //We need to show the text inside the span that the plugin show
      $('.bootstrap-select .filter-option').text(text);
      //Check the selected attribute for the real select
      $("#type_edit").val(response.type);

      $('#username_edit').val(response.username);
      $('#email_edit').val(response.email);
      $('#fullname_edit').val(response.name);
      // $("#type_edit").val(response.type);
      $('#'+response.gender).prop("checked",true);
      $('#tel_edit').val(response.tel);
      $('#address_edit').val(response.address);
      $('#birthday_edit').val(response.birthdate);
      $('#id_hidden').val(response.id);
      $('#image_edit').attr('src','https://s3-ap-southeast-1.amazonaws.com/seektour/img-user/'+response.img);


    },
    cache: false,
    contentType: false,
    processData: false
  });
}

$('#edit_admin').submit(function(){
  var fields = $('#edit_admin').find("select, textarea, input").serializeArray();
  var required=false;
  $.each(fields, function(i, field) {
    if (field.value==""){
      console.log(field);
      required=true;
    }
    if(field.name=="image"){
      required=false;
    }
  });
  if(required){
    console.log('false');
    return false;
  }
  $('#divLoading').addClass('show');
  $('#background').addClass('show');
  var formData = new FormData($(this)[0]);
  var table = $('#datatables').DataTable();
  $.ajax({
    url: '{{url('UserController')}}/'+$('#id_hidden').val()+'/update',
    type: 'POST',
    data: formData,
    dataType:'json',
    success : function (response) {
      // console.log(response);
      // alert(response);
      $('#divLoading').removeClass('show');
      $('#background').removeClass('show');
      $('#edit_admin').hide();

      swal("Successfully", "แก้ไข Admin เสร็จสิ้น", "success");
      table.clear();
      table.rows.add(response).draw();
      $('#edit_addmin').hide();
      baguetteBox.run('.tz-gallery');
      $('#edit_admin')[0].reset();
    },
    cache: false,
    contentType: false,
    processData: false
  });
  return false;
});
// $('#minimizeSidebar').click(function(){
//   $('body').addClass('slidebar-mini');
// });
</script>
@endsection
