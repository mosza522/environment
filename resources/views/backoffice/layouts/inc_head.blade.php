  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
  <link rel="stylesheet" href="{{asset('/assets/css/material-dashboard.css?v=2.0.1')}}">
  <!-- Documentation extras -->
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('assets/assets-for-demo/demo.css')}}" rel="stylesheet" />
  <!-- iframe removal -->
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}
  <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
  <link rel="stylesheet" href="{{asset('assets/grid/gallery-grid.css')}}">
  {{-- Grid gallert --}}
  <!-- Include Editor style. -->
  <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/froala_editor.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/froala_style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/code_view.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/colors.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/emoticons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/image.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/line_breaker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/table.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/char_counter.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/video.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/fullscreen.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/file.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/froalaEditor/css/plugins/quick_insert.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
