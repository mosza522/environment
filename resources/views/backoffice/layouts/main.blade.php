<!DOCTYPE html>
<html>
    <head>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!-- Favicons -->
  <link rel="apple-touch-icon" href="{{asset('assets/img/apple-icon.png')}}">
  <link rel="icon" href="{{asset('assets/img/favicon.png')}}">
  <title>
    @yield('title')
  </title>
  <!-- style -->
  @include('backoffice.layouts.inc_head')
  @include('backoffice.layouts.inc_js')

</head>
<body>
  <style media="screen">
   #divLoading.show
 {
   top: 50%;
   left: 50%;
   width:6em;
   height:6em;
   margin-top: -3em;
   margin-left: -3em;
   position:fixed;
   z-index: 9999;
   border: 10px solid #f3f3f3;
   border-radius: 50%;
   border-top: 10px solid #fff072;
   border-bottom: 10px solid #fff072;
   width: 90px;
   height: 90px;
   -webkit-animation: spin 2s linear infinite; /* Safari */
   animation: spin 2s linear infinite;

 }
 #background.show
 {
   width:100%;
   height:100%;
   position:fixed;
   z-index: 9999;
   background-color: rgb(0,0,0); /* Fallback color */
   background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
 }

 /* Safari */
 @-webkit-keyframes spin {
   0% { -webkit-transform: rotate(0deg); }
   100% { -webkit-transform: rotate(360deg); }
 }

 @keyframes spin {
   0% { transform: rotate(0deg); }
   100% { transform: rotate(360deg); }
 }
   </style>
   <div id="background">
  </div>
  <div id="divLoading">

  </div>

  <div class="wrapper">
     @include('backoffice.layouts.slidebar')
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute fixed-top">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo">{{Session::get('page')}}</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">dashboard</i>
                  <p>
                    <span class="d-lg-none d-md-block">Stats</span>
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions
                      <b class="caret"></b>
                    </span>

                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#pablo">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#pablo">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#pablo">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#pablo">Another Notification</a>
                  <a class="dropdown-item" href="#pablo">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <!--<span class="notification">5</span>-->
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions
                      <b class="caret"></b>
                    </span>

                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="{{url('logout')}}">logout</a>
                </div>
              </li>

              <!--<li class="nav-item">-->
              <!--  <a class="nav-link" href="#pablo">-->
              <!--    <i class="material-icons">person</i>-->
              <!--    <p>-->
              <!--      <span class="d-lg-none d-md-block">Account</span>-->
              <!--    </p>-->
              <!--  </a>-->
              <!--</li>-->
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          @yield('content')
        </div>
      </div>
                  <footer class="footer ">
                    <div class="container">
                      <nav class="pull-left">
                        <ul>
                          <li>
                            <a href="https://www.creative-tim.com">
                              Creative Tim
                            </a>
                          </li>
                          <li>
                            <a href="http://presentation.creative-tim.com">
                              About Us
                            </a>
                          </li>
                          <li>
                            <a href="http://blog.creative-tim.com">
                              Blog
                            </a>
                          </li>
                          <li>
                            <a href="https://www.creative-tim.com/license">
                              Licenses
                            </a>
                          </li>
                        </ul>
                      </nav>
                      <div class="copyright pull-right">
                        &copy;
                        <script>
                          document.write(new Date().getFullYear())
                        </script>, made with <i class="material-icons">favorite</i> by

                        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                      </div>
                    </div>
                  </footer>
                </div>
              </div>

</body>
</html>
