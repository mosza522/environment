@extends('backoffice.layouts.main')
@section('title')
  FAQ || Seektour
@endsection
<!-- Include JS file. -->
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.4/js/froala_editor.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
@section('content')

  <div class="row">
    <div class="col-md-12">
      <!-- Button trigger modal -->
      <div class="card">
        <div class="card-header card-header-rose card-header-icon">
          <div class="card-icon">
            <i class="material-icons">question_answer</i>
          </div>
          <h4 class="card-title">FAQ</h4>
        </div>
        <div class="card-body">
          <form id="FaqForm">
            {{csrf_field()}}
          <div class="col-md-12">
            <textarea id="froala-editor" name="content" rows="30" cols="80">
              @if (\App\Models\Content::where('type','faq')->count()>0)
                {!! \App\Models\Content::where('type','faq')->first()->content !!}
              @endif
            </textarea>
          </div>
          <div class="card-footer">
            <div class="col-md-12 text-right">
              <button type="reset" class="btn btn-danger btn-round close-add" >Close</button>
              <button type="submit" class="btn btn-success btn-round">บันทึก</button>
            </div>
          </div>
          </form>
        </div>
        <!-- end content-->
      </div>
      <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
  </div>

    <div class="row">
      <div class="col-md-12">
        <!-- Button trigger modal -->
        <div class="card">
          <div class="card-header card-header-rose card-header-icon">
            <div class="card-icon">
              <i class="material-icons">question_answer</i>
            </div>
            <h4 class="card-title">FAQ Example</h4>
          </div>
          <div class="card-body">
            <div class="col-md-12">
              <div id="example_faq">
                @if (\App\Models\Content::where('type','faq')->count()>0)
                  {!! \App\Models\Content::where('type','faq')->first()->content !!}
                @endif
              </div>
            </div>
          </form>
        </div>
        <!-- end content-->
      </div>
      <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
  </div>


  <script type="text/javascript">
    $('#FaqForm').submit(function(){
      $('#divLoading').addClass('show');
      $('#background').addClass('show');
      var formData = new FormData($(this)[0]);
      $.ajax({
        url: '{{url('ContentController')}}/faq',
        type: 'POST',
        data: formData,
        dataType:'JSON',
        success : function (response) {
          console.log(response);
          $('#example_faq').html(response.content);
          swal("Successfully", "บึนทึก FAQ เสร็จสิ้น", "success");
          // alert(response);
          $('#divLoading').removeClass('show');
          $('#background').removeClass('show');
          // $('#edit_admin').hide();
          //

          // table.clear();
          // table.rows.add(response).draw();
          // $('#edit_addmin').hide();
          // baguetteBox.run('.tz-gallery');
          // $('#edit_admin')[0].reset();
        },
        cache: false,
        contentType: false,
        processData: false
      });
      return false;
    });
  </script>
  <script type="text/javascript">
  // Get user's contry
  $.get("https://ipinfo.io", function(response) {
      console.log(response.city, response.country);
  }, "jsonp");
  //
  baguetteBox.run('.tz-gallery');

  $(function() {
    $('#froala-editor').froalaEditor({
      // toolbarButtons: ['undo', 'redo', 'html', '-', 'fontSize', 'paragraphFormat', 'align', 'quote', '|', 'formatOL', 'formatUL', '|', 'bold', 'italic', 'underline', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable'],
      height: 500,
      imageUploadURL: '{{ url('upload_image')}}/FAQ',
      imageUploadParams: {
        froala: 'true', // This allows us to distinguish between Froala or a regular file upload.
        _token: "{{ csrf_token() }}" // This passes the laravel token with the ajax request.
      },
      imageStyles: {
        class1: 'Class 1',
        class2: 'Class 2'
      },
      imageEditButtons: ['imageReplace', 'imageAlign', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize'],

    });
  });
  </script>

@endsection
