<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Hash;
use Session;
use Auth;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
      if($r->hasFile('image')) {
        $img = $r->file('image');
        $name_img= time().'-'.$img->getClientOriginalName();

      }
      $user= new \App\Models\User;
      $user->username=$r->username;
      $user->email=$r->email;
      $user->password=\Hash::make($r->password);
      $user->type=$r->type;
      $user->img=$name_img;
      $user->name=$r->fullname;
      $user->birthdate=$r->birthday;
      $user->gender=$r->gender;
      $user->tel=$r->tel;
      $user->address=$r->address;
      $user->created_by=\Auth::user()->id;
      if($user->save() and Storage::disk('s3')->put('img-user/'.$name_img, fopen($img, 'r+'), 'public')){
        $users= \App\Models\User::orderBy('id','DESC')->get();
        $return =array();
        $i=1;
        foreach ($users as $key ) {
          $storage=Storage::disk('s3')->has('img-user/'.$key->img);
          array_push($return,array($i++,$key->username,$key->name,$key->type,$key->email,$key->address,$key->tel,
        '<div class="tz-gallery">
          <a class="lightbox" href="'.($storage ?Storage::disk('s3')->url('img-user/'.$key->img) :asset('assets/grid/images/coast.jpg')).'">
            <img class="img-thumbnail" src="'.($storage ?Storage::disk('s3')->url('img-user/'.$key->img) :asset('assets/grid/images/coast.jpg')).'" alt="">
          </a>
        </div>',$key->gender,date('Y-m-d H:i:s',strtotime($key->created_at)),
        '<a href="#" class="btn btn-link btn-warning btn-just-icon edit" ><i class="material-icons">dvr</i></a>
        <a href="#" class="btn btn-link btn-danger btn-just-icon" onclick="return del('.$key->id.')"><i class="material-icons">close</i></a>'));
        }
        return response()->json($return);
      }else{
        echo "failed";
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \App\Models\User::find($id);
        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        // echo $id;
        $user= \App\Models\User::find($id);
        if($r->image){
          $img = $r->file('image');
          $name_img = time().'-'.$img->getClientOriginalName();
          $user->img=$name_img;
          if($user->img==""){
            Storage::disk('s3')->put('img-user/'.$name_img, fopen($img, 'r+'), 'public');
          }else{
            Storage::disk('s3')->delete('img-user/'.$user->img);
            Storage::disk('s3')->put('img-user/'.$name_img, fopen($img, 'r+'), 'public');
          }
        }
        $user->username=$r->username;
        $user->email=$r->email;
        $user->type=$r->type;
        $user->name=$r->fullname;
        $user->birthdate=$r->birthday;
        $user->gender=$r->gender;
        $user->tel=$r->tel;
        $user->address=$r->address;
        $user->updated_by=\Auth::user()->id;
        if($user->save()){
          $i=1;
          $return =array();
          $users= \App\Models\User::orderBy('id','DESC')->get();
          foreach ($users as $key ) {
            $storage=Storage::disk('s3')->has('img-user/'.$key->img);
            array_push($return,array($i++,$key->username,$key->name,$key->type,$key->email,$key->address,$key->tel,
          '<div class="tz-gallery">
            <a class="lightbox" href="'.($storage ?Storage::disk('s3')->url('img-user/'.$key->img) :asset('assets/grid/images/coast.jpg')).'">
              <img class="img-thumbnail" src="'.($storage ?Storage::disk('s3')->url('img-user/'.$key->img) :asset('assets/grid/images/coast.jpg')).'" alt="">
            </a>
          </div>',$key->gender,date('Y-m-d H:i:s',strtotime($key->created_at)),
          '<a href="#" class="btn btn-link btn-warning btn-just-icon edit" onclick="return show('.$key->id.')"><i class="material-icons">dvr</i></a>
          <a href="#" class="btn btn-link btn-danger btn-just-icon" onclick="return del('.$key->id.')"><i class="material-icons">close</i></a>'));
          }
          return response()->json($return);
        }


      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user= \App\Models\User::find($id);
      Storage::disk('s3')->delete('img-user/'.$user->img);
      $user->delete();

      $users= \App\Models\User::orderBy('id','DESC')->get();
      $return =array();
      $i=1;
      foreach ($users as $key ) {
        $storage=Storage::disk('s3')->has('img-user/'.$key->img);
        array_push($return,array($i++,$key->username,$key->name,$key->type,$key->email,$key->address,$key->tel,
      '<div class="tz-gallery">
        <a class="lightbox" href="'.($storage ?Storage::disk('s3')->url('img-user/'.$key->img) :asset('assets/grid/images/coast.jpg')).'">
          <img class="img-thumbnail" src="'.($storage ?Storage::disk('s3')->url('img-user/'.$key->img) :asset('assets/grid/images/coast.jpg')).'" alt="">
        </a>
      </div>',$key->gender,date('Y-m-d H:i:s',strtotime($key->created_at)),
      '<a href="#" class="btn btn-link btn-warning btn-just-icon edit" onclick="return show('.$key->id.')"><i class="material-icons">dvr</i></a>
      <a href="#" class="btn btn-link btn-danger btn-just-icon" onclick="return del('.$key->id.')"><i class="material-icons">close</i></a>'));
      }
      return response()->json($return);

    }

}
