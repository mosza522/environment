<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Content;
use Auth;
class ContentController extends Controller
{
  public function store(request $r, $type)
  {
      if (Content::where('type',$type)->count()>0) {
        $content = Content::where('type',$type)->first();
        $content->content = $r->content;
        $content ->type = $type;
        $content ->updated_by = \Auth::user()->id;
        $content ->save();
        return response()->json($this->show($type));
      }else{
        $content = new Content;
        $content ->content = $r->content;
        $content ->type = $type;
        $content ->created_by = \Auth::user()->id;
        $content ->save();
        return response()->json($this->show($type));
      }

  }
  public function show($type)
  {
    return Content::where('type',$type)->first();
  }
}
