<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/backoffice';
// protected function authenticated(Request $request, $user)
//     {
//     if ( !Auth::guest() and Auth::user()->type=='Admin' ) {// do your margic here
//         return redirect('/backoffice');
//     }else{
//         return redirect('/');
//     }


//     }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
  	{

        return redirect('/');
  	    //  if(Session::has('usertype')){
  	    //      if(Session::get('usertype')=="Admin"){
  	    //         #foradmin
  	    //         return redirect('/loginBackoffice');
  	    //      }else{
  	    //         #foruser
  	    //         return redirect('/loginUser');
  	    //      }
  	    //  }else{
  	    //     #foruser
  	    //     return redirect('/loginUser');
  	    //  }


  	}
    // public function authenticate(request $request)
    // {
    //   $username = $request->username; //the input field has name='username' in form
    //   $password = $request->password;
    //   if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
    //     //user sent their email
    //     Auth::attempt(['email' => $username, 'password' => $password]);
    //   } else {
    //     //they sent their username instead
    //     Auth::attempt(['username' => $username, 'password' => $password]);
    //   }
    //
    //   //was any of those correct ?
    //   if ( Auth::check() ) {
    //     //send them where they are going
    //     return redirect('/');
    //   }
    //
    //   //Nope, something wrong during authentication
    //   return redirect()->back()->withErrors([
    //     'credentials' => 'ไม่พบข้อมูลในระบบ'
    //   ]);
    // }
    public function username()
  	{
  			return 'username';
  	}
  	public function logout() {
  	//   $session=Auth::user()->type;
      Auth::logout();
      Session::flush();
    //   Session::put('usertype',$session);
      return redirect('/');
    }
}
