<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    if(auth()->user()->type == "SuperAdmin" or auth()->user()->type == "Admin"){
    return $next($request);
    }
    Auth::logout();
    Session::flush();
    // Session::flash('wrong','ไม่มีสิทธิ์เข้าถึง');
    return redirect('/')->withErrors(['username'=>'Only for Superadmin or admin.']);
    }
}
