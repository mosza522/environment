<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FroalaImages extends Model
{
  protected $table	= 'froala_images';
  public $timestamps 	= false;
}
