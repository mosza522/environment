<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
  $admin=$faker->unique()->numberBetween(0,100);
    return [
        'username'=>$faker->text(10),
        'email' => $faker->unique()->safeEmail,
        'name' => $faker->name,
        'type' => $faker->randomElement(['Admin','User']),
        'address' => $faker->address,
        'tel' => $faker->phoneNumber,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'img' => $faker->imageUrl($width = 500,$height = 500),
        'created_by'=> $admin,
        'updated_by'=> $admin,
        'gender' => $faker->randomElement(['Male','Female']),
        'age'=> $faker->numberBetween(0,100),
    ];
});
