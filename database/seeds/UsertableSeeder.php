<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class UsertableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class,20)->create();
    }
}
